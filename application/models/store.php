<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class store extends CI_Model{

	public function __construct()
	{
	parent::__construct();
	}
	/************************************************************************
	tabel store
	************************************************************************/
	public function get_store(){
	$query=$this->db->get('store');
	return $query->result();

	}
	public function get_store_by_filter($filter){
	$this->db->from('store');
	if($filter['search']!='')
	$this->db->where('nama_store like \'%'.$filter['search'].'%\' ');
	if($filter['lokasi']!='')
	$this->db->where('lokasi='.$filter['lokasi']);
	$query=$this->db->get();
	return $query->result();
	}
	public function get_store_by_user($user){
	
		$this->db->where('id_user',$user);
		$query=$this->db->get('store');
		
		return $query->result();
	}
/******************************************************************
tabel produk
******************************************************************/
	public function get_produk($filter){
		$condition=array();
		if($filter['cat']!=''&& $filter['cat']!='null')
		$condition['kategori.id_kategori']=$filter['cat'];
		if($filter['lokasi']!=''&& $filter['lokasi']!='null')
		$condition['lokasi']=$filter['lokasi'];
		
		$this->db->where($condition);
		$this->db->where('(nama_produk like \'%'.$filter['search'].'%\' or produk.deskripsi like \'%'.$filter['search'].'%\' )');
		$this->db->from('produk');
		$this->db->join('store','produk.id_store=store.id_store');
		$this->db->join('prod_cat','prod_cat.id_produk=produk.id_produk');
		$this->db->join('kategori','kategori.id_kategori=prod_cat.id_kategori');
		$this->db->group_by('produk.id_produk');

	$query=$this->db->get();
	return $query->result();

	}
	public function get_product_by_store($id_store){
	$this->db->where('id_store',$id_store);
	$query=$this->db->get('produk');
	return $query->result();

	}
	
	public function get_produk_by_filter($filter){
		
		$query=$this->db->select('produk.*','prod_cat.*', 'kategori.nama_kategori');
	$query=$this->db->from('produk,prod_cat,kategori');
	 $this->db->where('produk.id_produk=prod_cat.id_produk and prod_cat.id_kategori = kategori.id_kategori');
		if($filter['search']!='')
		$this->db->where('nama_produk like \'%'.$filter['search'].'%\' ');
		if($filter['cat']!='')
		$this->db->where('');
	
	
	$query=$this->db->get();
	return $query->result();

	}
	/******************************************************************************
	tabel kategori
	*******************************************************************************/
	public function get_kategori(){
	$query=$this->db->get('kategori');
	return $query->result();

	}

}